package dan.ca.pickerdialog;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class MainActivity extends Activity implements NumberDialog.OnNumberDialogDoneListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button= (Button) findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        NumberDialog myDiag = NumberDialog.newInstance(3, 123);
                        myDiag.show(getFragmentManager(), "Diag");
                    }
                });
    }

    @Override
    public void onDone(int value) {
        TextView tv = (TextView)findViewById(R.id.textView);
        tv.setText(Integer.toString(value));
    }
}
